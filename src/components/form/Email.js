import React from 'react';

const Email = (props) => {
    return (
        <>
            <label htmlFor="email">Email</label>
            <input type="email"
                name="email"
                id="email"
                className="form-control email"
                {...props}
            />
        </>
    );
};

export default Email;