import React from 'react';

const Password = (props) => {
    return (
        <>
            <label htmlFor="password">Password</label>
            <input type="password" id="password" className="form-control password" {...props} />
        </>
    );
};

export default Password;