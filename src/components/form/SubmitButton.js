import React from 'react';

const SubmitButton = ({ label, ...props }) => {
    return (
        <button type="submit" className="btn btn-primary" {...props}>{label}</button>
    );
};

export default SubmitButton;