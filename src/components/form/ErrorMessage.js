import React from 'react';

const ErrorMessage = ({ text }) => {

    const style = text.length > 0 ? { display: 'block' } : { display: 'none' };

    return (
        <div className={"alert alert-danger"} style={style} role="alert">
            {text}
        </div>
    );
};

export default ErrorMessage;