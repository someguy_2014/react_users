import React from 'react';
import "./LoadingOverlay.css"

const LoadingOverlay = ({ text, active }) => {

    const style = active ? { display: 'block' } : {};

    return (
        <div id="overlay-spin" style={style}>{text}</div>
    );
}

export default LoadingOverlay;