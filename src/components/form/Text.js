import React from 'react';

const Text = ({ id, label, ...props }) => {
    return (
        <>
            <label htmlFor={id}>{label}</label>
            <input type="text" id={id} className="form-control" {...props} />
        </>
    );
};

export default Text;