import React from 'react';
import Password from './form/Password';
import Email from './form/Email';
import Text from './form/Text';
import SubmitButton from './form/SubmitButton';
import LoadingOverlay from './form/LoadingOverlay';
import ErrorMessage from './form/ErrorMessage';

const UserForm = (props) => {

    const { fields, status, error, setFormFieldValue, handleSubmit } = props;

    return (
        <div className="container wrapper">
            <div className="card">
                <div className="card-header">
                    <h4 className="mb-0">User Information</h4>
                </div>
                <div className="card-body">
                    <form onSubmit={handleSubmit} noValidate className={status.validated ? 'was-validated' : ''}>
                        <LoadingOverlay active={status.submitting} />
                        <ErrorMessage text={error.message} />
                        <div className="form-row">
                            <div className="form-group col-6">
                                <Text label="First name" id="firstName" required autoFocus value={fields.firstName} onChange={setFormFieldValue} />
                            </div>
                            <div className="form-group col-6">
                                <Text label="Last name" id="lastName" required value={fields.lastName} onChange={setFormFieldValue} />
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-6">
                                <Email required value={fields.email} onChange={setFormFieldValue} />
                            </div>
                            <div className="form-group col-6">
                                <Password required value={fields.password} onChange={setFormFieldValue} />
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-12">
                                <SubmitButton label="Submit" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default UserForm;