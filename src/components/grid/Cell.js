import React from 'react'

const Cell = (props) => {
    const { css, label } = props;

    return <div className={"col text-wrap " + css}>{label}</div>
}

export default Cell;