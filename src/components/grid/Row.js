import React from 'react'
import Cell from './Cell';

const Row = (props) => {
    const { grid, rowData, rowIndex } = props;
    const cells = grid.columns.map((column, cellIndex) => {
        const { css, field, render } = column;
        let cellContent, cellContentType;

        const cell = (cellContent, cellContentType) => {
            const key = [name, "cell", cellContentType, rowIndex, cellIndex].join('-');

            return <Cell key={key} css={css} label={cellContent} />
        }

        if (render) {
            cellContent = render(rowData);
            cellContentType = 'rendered';
        } else {
            cellContentType = field;
            cellContent = rowData[field];
        }

        return cell(cellContent, cellContentType)

    });

    return <div className="row no-gutters">{cells}</div>
}



export default Row;
