import React from 'react'
import Cell from './Cell';

const Header = (props) => {
    const { name: name, columns } = props;

    const cells = columns.map((column, index) => {
        const key = [name, 'header', column, index].join('-');

        return <Cell key={key} {...column} />
    });

    return <div className="row no-gutters">{cells}</div>
}



export default Header;