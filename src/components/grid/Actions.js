import React from 'react'
import './Actions.css'

const Actions = ({ grid, rowData }) => {
    return (
        <div className="flex">
            <div><a href="" onClick={(e) => { return grid.handleEditRow(e, rowData) }}><i className="fas fa-edit"></i></a> </div>
            <div><a href="" onClick={(e) => { return grid.handleDeleteRow(e, rowData) }}><i className="fas fa-trash"></i></a> </div>
        </div>
    )
}

export default Actions;
