import React from 'react'
import './Grid.css'
import Header from './Header';
import Row from './Row';

const Grid = (props) => {

    const { name, rowsData, grid } = props;
    const { columns } = grid;
    const headerKey = [name, 'header', 'row'].join('-');
    const rows = rowsData.map((rowData, rowIndex) => {
        const key = [name, "row", rowIndex].join('-');

        return <Row key={key} rowIndex={rowIndex} grid={grid} rowData={rowData} />
    });

    return (
        <div className="table-container p-0 m-0">
            <Header key={headerKey} {...{ name, columns }} />
            {rows}
        </div>
    )
}

export default Grid;