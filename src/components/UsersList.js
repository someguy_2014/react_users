import React, { useEffect, useState } from 'react'
import { useHistory, Link } from "react-router-dom";
import { fetchUsers, deleteUser } from '../libs/API';
import Grid from './grid/Grid';
import Actions from './grid/Actions';
import LoadingOverlay from './form/LoadingOverlay';
import ErrorMessage from './form/ErrorMessage';
import NewUser from '../containers/NewUser';

const initialUsers = [];
const grid = {
    columns: [
        { field: 'id', label: '#', css: 'col-xs-1 col-lg-1' },
        { field: 'first_name', label: 'First name', css: 'col-xs-1 col-lg-3' },
        { field: 'last_name', label: 'Last name', css: 'col-xs-1 col-lg-3' },
        { field: 'email', label: 'e-mail', css: 'col-xs-1 col-lg-4' },
        { label: 'Actions', css: 'col-xs-1 col-lg-1', render: (user) => { return <Actions {...{ grid }} rowData={user} /> } }
    ],
    actions: { handleEditRow: undefined, handleDeleteRow: undefined }
};

const UsersList = () => {
    const history = useHistory();
    const [users, setUsers] = useState(initialUsers);
    const [addNewUser, setAddNewUser] = useState(false);
    const [overlay, setOverlay] = useState(false);
    const [error, setError] = useState('');

    useEffect(() => {
        setOverlay(true);
        fetchUsers().then(response => {
            const users = response.data.map(user => {
                const [first_name, last_name] = user.name.split(" ");

                return { ...user, first_name, last_name };
            });
            setUsers(users);
        }).catch(error => {
            setError(error.message);
        }).then(() => { setOverlay(false); });
    }, []);

    grid.handleEditRow = (e, user) => {
        e.preventDefault();
        history.push('/users/' + user.id);
        return false;
    }
    grid.handleDeleteRow = (e, user) => {
        e.preventDefault();
        setOverlay(true);
        deleteUser(user.id).then(() => {
            setUsers(users.filter(currentUser => currentUser.id !== user.id));
        }).catch(error => {
            setError(error.message);
        }).then(() => { setOverlay(false); });

        return false;
    }

    const renderGrid = () => {
        return (
            <div className="container wrapper">
                <div className="row p-1 no-gutters">
                    <div className="col"><h4>Users</h4></div>
                    <div className="col text-right">
                        <button className="btn btn-primary" onClick={() => { setAddNewUser(true); }}>Add user</button>
                    </div>
                </div>
                <div className="row no-gutters">
                    <LoadingOverlay active={overlay} />
                    <ErrorMessage text={error} />
                </div>
                <div className="row no-gutters">
                    <Grid key="users_grid" name="users" rowsData={users} grid={grid} />
                </div>
            </div>
        )
    }

    const renderNewUser = () => {
        return <NewUser redirect={() => { setAddNewUser(false); }} />
    }

    return !addNewUser ? renderGrid() : renderNewUser();
}

export default UsersList;
