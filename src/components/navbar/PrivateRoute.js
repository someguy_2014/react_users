import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useAuth } from "../../libs/useAuth";

const PrivateRoute = ({ component: Component, ...rest }) => {

    const { authTokens } = useAuth();

    const component = (props) => {
        return authTokens ? <Component {...props} /> : <Redirect to={{ pathname: "/login", state: { referer: props.location } }} />
    }

    return <Route {...rest} render={props => { return component(props) }} />;
}

export default PrivateRoute;
