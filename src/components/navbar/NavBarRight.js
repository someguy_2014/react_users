import React from 'react'
import { Link, useHistory } from 'react-router-dom';
import { useAuth } from '../../libs/useAuth';

const NavBarRight = () => {
    const { authTokens, setAuthTokens } = useAuth();
    const history = useHistory();

    const handleLogOut = (e) => {
        e.preventDefault();
        setAuthTokens();
        history.push('/');
    }

    const logout = () => {
        return (
            <li className="nav-item">
                <Link to="/logout" className="nav-link" onClick={handleLogOut}><i className="fas fa-user"></i> Log Out</Link>
            </li>
        );
    }
    const login = () => {
        return (
            <>
                <li className="nav-item">
                    <Link to="/signup" className="nav-link"><i className="fas fa-user"></i> Sign Up</Link>
                </li>
                <li className="nav-item">
                    <Link to="/login" className="nav-link"><i className="fas fa-sign-in-alt"></i> Login</Link>
                </li>
            </>
        );
    }
    return authTokens ? logout() : login();
}

export default NavBarRight;
