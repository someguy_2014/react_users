import React from 'react'
import { useAuth } from '../../libs/useAuth';
import { Link } from 'react-router-dom';

const PrivateNavLink = ({ to, label }) => {
    const { setAuthTokens } = useAuth();
    const emptyLink = <></>;
    const navlink = (
        <li className="nav-item">
            <Link className="nav-link" to={to}>{label}</Link>
        </li>
    );

    return setAuthTokens ? navlink : emptyLink;
}

export default PrivateNavLink;
