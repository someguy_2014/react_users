import React from 'react'
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import Login from '../../containers/Login';
import Home from '../../containers/Home';
import NewUser from '../../containers/NewUser';
import Users from '../../containers/Users';
import About from '../../containers/About';
import EditUser from '../../containers/EditUser';
import PrivateRoute from './PrivateRoute';
import PrivateNavLink from './PrivateNavItem';
import NavBarRight from './NavBarRight';

const NavBar = () => {
    return (
        <BrowserRouter>
            <nav className="navbar navbar-inverse navbar-expand-lg navbar-light bg-light">
                <Link className="navbar-brand" to="/">123it.com</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link className="nav-link" to="/">Home <span className="sr-only">(current)</span></Link>
                        </li>
                        <PrivateNavLink to='/users/'>Users</PrivateNavLink>
                    </ul>
                    <ul className="navbar-nav navbar-right">
                        <NavBarRight />
                    </ul>
                </div>
            </nav>

            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/signup" component={NewUser} />
                <Route path="/login" component={Login} />
                <PrivateRoute exact path="/users" component={Users} />
                <PrivateRoute path="/users/:id" component={EditUser} />
            </Switch>
        </BrowserRouter>
    );
};

export default NavBar;
