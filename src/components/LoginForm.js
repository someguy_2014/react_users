import React from "react";
import Email from "./form/Email";
import Password from "./form/Password";
import SubmitButton from "./form/SubmitButton";
import LoadingOverlay from "./form/LoadingOverlay";
import ErrorMessage from "./form/ErrorMessage";
import "./LoginForm.css";

const LoginForm = (props) => {

    const { fields, status, error, setFormFieldValue, handleSubmit } = props;

    return (
        <div className="login-container">
            <form onSubmit={handleSubmit} className={status.validated ? 'was-validated' : ''} noValidate>
                <LoadingOverlay active={status.submitting} />
                <ErrorMessage text={error.message} />
                <div className="form-group">
                    <Email value={fields.email} autoFocus onChange={setFormFieldValue} required />
                </div>
                <div className="form-group">
                    <Password value={fields.password} onChange={setFormFieldValue} required />
                </div>
                <SubmitButton label="Login" />
            </form>
        </div>
    );
};

export default LoginForm;
