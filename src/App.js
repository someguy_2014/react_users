import React, { useState } from 'react';
import NavBar from './components/navbar/NavBar';
import { AuthContext } from './libs/useAuth';

const App = (props) => {

    const existingTokens = JSON.parse(localStorage.getItem("tokens"));
    const [authTokens, setAuthTokens] = useState(existingTokens);

    const setTokens = (data) => {
        if (data) {
            localStorage.setItem("tokens", JSON.stringify(data));
        } else {
            localStorage.removeItem("tokens");
        }
        setAuthTokens(data);
    }

    return (
        <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
            <NavBar />
        </AuthContext.Provider>
    );
};

export default App;
