import { useState } from 'react'

const defaultFormProps = {
    handleSubmit: undefined,
    status: {
        submitting: false,
        validated: false,
    },
    error: {
        message: ''
    }
};

const useForm = (handleSubmitForm) => {

    const { status, error } = defaultFormProps;
    const [validated, setValidated] = useState(status.validated);
    const [submitting, setSubmitting] = useState(status.submitting);
    const [formErrorMessage, setFormErrorText] = useState(error.message);

    const handleSubmit = (event) => {
        event.preventDefault();
        event.stopPropagation();

        const valid = event.target.checkValidity();
        setValidated(true);
        valid && submit();

        return false;
    }

    const submit = () => {
        setSubmitting(true);
        handleSubmitForm().catch(handleSubmitError);
    }

    const handleSubmitError = error => {
        setFormErrorText(error.message);
        setSubmitting(false);
    }

    return {
        handleSubmit,
        status: { submitting, validated },
        error: { message: formErrorMessage }
    }
}

export default useForm;
