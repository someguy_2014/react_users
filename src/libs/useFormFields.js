import { useState, useCallback } from "react";

const useFormFields = (initialState) => {
    const [fields, setFormFields] = useState(initialState);
    const onChange = (e) => {
        const { id, value } = e.target;
        setFormFields((prev) => {
            return { ...prev, [id]: value }
        });
    };

    return [fields, onChange, setFormFields];
}

export default useFormFields