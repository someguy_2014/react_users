import axios from 'axios';

const API = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com',
  responseType: 'json'
});

export const fetchUsers = () => {
  return API.get('/users');
};

export const fetchUserById = (id) => {
  return API.get('/users/' + id);
};

export const createUser = (userData) => {
  const data = userDataForRequest(userData);

  return API.post('/users', data);
};

export const updateUser = (id, userData) => {
  const data = userDataForRequest(userData);

  return API.put('/users/' + id, { ...data });
};

export const deleteUser = (id) => {
  return API.delete('/users/' + id);
};
const userDataForRequest = userData => {
  const { firstName: first_name, lastName: last_name, email: username, password } = userData

  return { first_name, last_name, username, password }
};