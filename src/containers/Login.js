import React, { useState } from "react";
import LoginForm from "../components/LoginForm";
import useForm from "../libs/useForm";
import { useHistory } from "react-router-dom";
import useFormFields from "../libs/useFormFields";
import { fetchUsers } from "../libs/API";
import { useAuth } from '../libs/useAuth';

const fields = { email: '', password: '' };

const Login = (props) => {

    const { authTokens, setAuthTokens } = useAuth();
    const history = useHistory();
    const [formFields, setFormFieldValue] = useFormFields(fields);
    const handleSubmitForm = () => { return fetchUsers().then(handleSubmitResponse) };
    const handleSubmitResponse = response => {
        const users = response.data;
        const user = users.filter(filterUser)[0];

        if (user == undefined) throw new Error('User not found!');

        setAuthTokens({ token: user.id });
        history.push('/users');
    };
    const filterUser = user => { return user.email.toLowerCase() == formFields.email.toLowerCase(); };
    const formProps = useForm(handleSubmitForm);

    return (
        <LoginForm {...formProps} setFormFieldValue={setFormFieldValue} fields={formFields} />
    );

};
export default Login;
