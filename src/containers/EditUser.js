import React, { useEffect } from 'react';
import { useHistory, useParams } from "react-router-dom";
import useFormFields from '../libs/useFormFields';
import { updateUser, fetchUserById } from '../libs/API';
import UserForm from '../components/UserForm';
import useForm from '../libs/useForm';

const EditUser = () => {

    const { id } = useParams();
    const history = useHistory();
    const [formFields, setFormFieldValue, setFormFields] = useFormFields({ firstName: '', lastName: '', email: '', password: '' });
    const handleSubmitForm = () => { return updateUser(id, { ...formFields }).then(() => { history.push('/users'); }) };
    const formProps = useForm(handleSubmitForm);

    useEffect(() => {
        fetchUserById(id).then(response => {
            const user = response.data;
            const { name, email, password = 'change me' } = user;
            const [firstName, lastName] = name.split(' ');

            setFormFields({ firstName, lastName, email, password });
        });
    }, [id]);

    return (
        <UserForm {...formProps} setFormFieldValue={setFormFieldValue} fields={formFields} />
    );
};

export default EditUser;