import React from 'react';
import { useHistory } from "react-router-dom";
import useFormFields from '../libs/useFormFields';
import { createUser } from '../libs/API';
import UserForm from '../components/UserForm';
import useForm from '../libs/useForm';

const NewUser = (props) => {

    const history = useHistory();
    const { redirect = () => { history.push('/login'); } } = props;
    const [formFields, setFormFieldValue] = useFormFields({ firstName: '', lastName: '', email: '', password: '' });
    const handleSubmitForm = () => { return createUser(formFields).then(() => { redirect() }) };
    const formProps = useForm(handleSubmitForm);

    return (
        <UserForm {...formProps} setFormFieldValue={setFormFieldValue} fields={formFields} />
    );
};

export default NewUser;
